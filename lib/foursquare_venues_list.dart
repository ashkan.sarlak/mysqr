import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mysqr/bloc/bloc_provider.dart';
import 'package:mysqr/bloc/search_area_bloc.dart';
import 'package:mysqr/data_layer/model/venue.dart';
import 'package:mysqr/venue_detail_screen.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class FoursquareVenueList extends StatefulWidget {
  @override
  _FoursquareVenueListState createState() => _FoursquareVenueListState();
}

class _FoursquareVenueListState extends State<FoursquareVenueList> {
  StreamSubscription sub;

  final _itemScrollController = ItemScrollController();

  SearchAreaBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<SearchAreaBloc>(context);
    sub = bloc.selectedVenueStream.listen(
      (index) {
        print('bottom list got the index: $index');
        if (index < 0) {
          // when nothing is selected, like on a fresh search.
          return;
        }
        _itemScrollController.scrollTo(
          index: index,
          alignment: 0.3,
          duration: Duration(milliseconds: 300),
          curve: Curves.easeInOutCubic,
        );
        setState(() {
          /* Just update the selected Card's color. */
        });
      },
    );
  }

  @override
  void dispose() {
    sub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Venue>>(
        stream: BlocProvider.of<SearchAreaBloc>(context).venueStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return AnimatedOpacity(
              opacity: snapshot.data.length > 0 ? 1.0 : 0.0,
              duration: Duration(milliseconds: 1000),
              curve: Interval(0.5, 1.0),
              child: ScrollablePositionedList.builder(
                scrollDirection: Axis.horizontal,
                itemCount: snapshot.data.length,
                itemScrollController: _itemScrollController,
                itemBuilder: (BuildContext context, int index) {
                  final isSelected = index == bloc.lastSelectedVenueIndex;
                  final venueData = snapshot.data[index];
                  return Container(
                    width: 162,
                    padding: EdgeInsets.only(bottom: 8),
                    child: Card(
                      elevation: 4,
                      color: isSelected ? Colors.green[400] : null,
                      child: InkWell(
                        onTap: () {
                          print('Tapped Card index: $index');
                          if (isSelected) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    VenueDetailScreen(venueData),
                              ),
                            );
                          } else {
                            bloc.selectVenue(index);
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Image.network(
                                venueData.categories[0].icon.noBg32,
                                color:
                                    isSelected ? Colors.white : Colors.black54,
                                width: 24,
                                height: 24,
                              ),
                              Text(
                                venueData.name,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: isSelected
                                      ? Colors.white
                                      : Colors.black54,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            );
          }
          return Container();
        });
  }
}
