import 'dart:async';

import 'package:mysqr/bloc/adapter/map_data.dart';
import 'package:mysqr/bloc/bloc.dart';
import 'package:mysqr/data_layer/model/venue.dart';
import 'package:mysqr/di/remote_client_provider.dart';
import 'package:rxdart/rxdart.dart';

class SearchAreaBloc implements Bloc {
  final _searchButtonController = StreamController.broadcast();

  Stream get searchButtonPressedStream => _searchButtonController.stream;

  final _venueController = StreamController<List<Venue>>.broadcast();

  Stream get venueStream => _venueController.stream;

  final _selectedVenueIndexController = BehaviorSubject<int>();

  Stream get selectedVenueStream => _selectedVenueIndexController.stream;

  final _isSearchingController = BehaviorSubject.seeded(false);

  Stream get isSearchingStream => _isSearchingController.stream;

  // -1 when nothing selected, as indices usually start from 0.
  int get lastSelectedVenueIndex => _selectedVenueIndexController.hasValue
      ? _selectedVenueIndexController.value
      : -1;

  void searchThisArea() {
    _selectedVenueIndexController.add(-1); // no venue is selected.
    _venueController.add(List()); // The previous list is not valid anymore.
    _searchButtonController.add(null);
    _isSearchingController.add(true);
  }

  Future<void> queryMapData(MapData mapData) async {
    final venues = await RemoteClientProvider().client.fetchVenues(mapData);
    _venueController.add(venues);
    _isSearchingController.add(false);
  }

  void selectVenue(int index) {
    _selectedVenueIndexController.add(index);
  }

  @override
  void dispose() {
    _searchButtonController.close();
    _venueController.close();
    _selectedVenueIndexController.close();
    _isSearchingController.close();
  }
}
