import 'dart:async';

import 'package:mysqr/bloc/bloc.dart';
import 'package:mysqr/data_layer/model/venue.dart';
import 'package:mysqr/data_layer/model/venue_detail.dart';
import 'package:mysqr/di/remote_client_provider.dart';

class VenueDetailBloc implements Bloc {
  final _venueDetailController = StreamController<VenueDetail>.broadcast();

  Stream get venueDetailStream => _venueDetailController.stream;

  Future<void> getDetailsOf(Venue venue) async {
    final venueDetail =
        await RemoteClientProvider().client.fetchVenueDetails(venue);
    // Controller gets closed in case the user close the details page fast.
    if (!_venueDetailController.isClosed) {
      _venueDetailController.add(venueDetail);
    }
  }

  @override
  void dispose() {
    _venueDetailController.close();
  }
}
