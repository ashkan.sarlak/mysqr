import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

/// A wrapper/adapter which captures map data (e.g. google map region) and
/// converts it to make it usable by another class (e.g. foursquare queries).
class MapData {
  final LatLngBounds _visibleRegion;

  MapData(this._visibleRegion);

  /// This method and the next calculate center of visible region, i.e. user's
  /// location in eyes of Foursquare.
  double get userLatitude {
    return (_visibleRegion.southwest.latitude +
            _visibleRegion.northeast.latitude) /
        2;
  }

  double get userLongitude {
    return (_visibleRegion.southwest.longitude +
            _visibleRegion.northeast.longitude) /
        2;
  }

  String get userLatLng => '$userLatitude,$userLongitude';

  /// Radius of visible region's circumscribed circle, which is equal to half of
  /// region's diagonal.
  double get searchRadius {
    return Geolocator.distanceBetween(
            _visibleRegion.southwest.latitude,
            _visibleRegion.southwest.longitude,
            _visibleRegion.northeast.latitude,
            _visibleRegion.northeast.longitude) /
        2;
  }
}
