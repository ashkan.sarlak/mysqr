import 'package:flutter/material.dart';
import 'package:mysqr/bloc/bloc_provider.dart';
import 'package:mysqr/bloc/venue_detail_bloc.dart';
import 'package:mysqr/custom_circular_progress_indicator.dart';
import 'package:mysqr/data_layer/model/user.dart';
import 'package:mysqr/data_layer/model/venue.dart';
import 'package:mysqr/data_layer/model/venue_detail.dart';
import 'package:transparent_image/transparent_image.dart';

const _imageWidth = 500;
const _imageHeight = 400;

class VenueDetailScreen extends StatefulWidget {
  final Venue venue;

  VenueDetailScreen(this.venue);

  @override
  _VenueDetailScreenState createState() => _VenueDetailScreenState();
}

class _VenueDetailScreenState extends State<VenueDetailScreen> {
  final bloc = VenueDetailBloc();

  @override
  void initState() {
    super.initState();
    bloc.getDetailsOf(widget.venue);
  }

  @override
  void dispose() {
    // As we aren't using a BlocProvider here, we have to dispose it manually.
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: bloc,
      child: Scaffold(
        body: Container(
          color: Colors.white,
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              AspectRatio(
                aspectRatio: _imageWidth / _imageHeight,
                child: IntrinsicWidth(
                  child: Container(
                    child: Stack(
                      children: [
                        BestPhoto(),
                        Align(
                          child: CategoryIcons(),
                          alignment: Alignment.bottomRight,
                        ),
                        Align(
                          child: LikeCount(),
                          alignment: Alignment.bottomLeft,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              NameAndAddress(venue: widget.venue),
              Contact(venue: widget.venue),
              HereNowAndStats(venue: widget.venue),
              PhotosList(),
              TipsList(),
            ],
          ),
        ),
      ),
    );
  }
}

class BestPhoto extends StatelessWidget {
  const BestPhoto({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect bounds) {
        return LinearGradient(
          begin: Alignment(0.0, 0.7),
          end: Alignment(0.0, 1.0),
          colors: [Colors.transparent, Colors.white],
          tileMode: TileMode.clamp,
        ).createShader(Rect.fromLTRB(0, 0, bounds.width, bounds.height));
      },
      blendMode: BlendMode.srcOver,
      child: StreamBuilder<VenueDetail>(
        stream: BlocProvider.of<VenueDetailBloc>(context).venueDetailStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final bestPhoto = snapshot.data.bestPhoto;
            return bestPhoto == null
                ? Center(
                    child: Icon(Icons.no_photography_outlined),
                  )
                : Container(
                    child: SizedBox.expand(
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: FadeInImage.memoryNetwork(
                          image: bestPhoto.size(
                            width: _imageWidth,
                            height: _imageHeight,
                          ),
                          placeholder: kTransparentImage,
                        ),
                      ),
                    ),
                  );
          } else {
            return Center(
              child: CustomCircularProgressIndicator(
                color: Colors.black,
                size: 24.0,
              ),
            );
          }
        },
      ),
    );
  }
}

class CategoryIcons extends StatelessWidget {
  const CategoryIcons({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<VenueDetail>(
        stream: BlocProvider.of<VenueDetailBloc>(context).venueDetailStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: snapshot.data.categories
                  .map((e) => Card(
                        elevation: 20,
                        color: Colors.green[800],
                        child: Image.network(e.icon.noBg32),
                      ))
                  .toList(),
            );
          } else {
            return Container();
          }
        });
  }
}

class LikeCount extends StatelessWidget {
  const LikeCount({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<VenueDetail>(
        stream: BlocProvider.of<VenueDetailBloc>(context).venueDetailStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Card(
              elevation: 20,
              color: Colors.red,
              child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.favorite_border,
                      color: Colors.white,
                    ),
                    Text(
                      '${snapshot.data.likes.count}',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12.0,
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Container();
          }
        });
  }
}

class NameAndAddress extends StatelessWidget {
  const NameAndAddress({
    Key key,
    @required this.venue,
  }) : super(key: key);

  final Venue venue;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    venue.name,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(width: 8),
                Icon(
                  Icons.verified,
                  color: venue.verified ? Colors.green : Colors.grey[300],
                ),
              ],
            ),
            Row(
              children: [
                Icon(
                  Icons.location_on,
                  color: Colors.grey[700],
                  size: 16.0,
                ),
                SizedBox(width: 8),
                Expanded(
                  child: Text(
                    venue.location.formattedAddress.join(', '),
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Contact extends StatelessWidget {
  const Contact({
    Key key,
    @required this.venue,
  }) : super(key: key);

  final Venue venue;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          children: [
            Icon(
              Icons.phone,
              color: Colors.grey[700],
              size: 16.0,
            ),
            SizedBox(width: 8),
            StreamBuilder<VenueDetail>(
                stream:
                    BlocProvider.of<VenueDetailBloc>(context).venueDetailStream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(snapshot.data.contact?.formattedPhone ??
                        'Not available');
                  } else {
                    return CustomCircularProgressIndicator(
                        color: Colors.grey[700]);
                  }
                }),
          ],
        ),
      ),
    );
  }
}

class HereNowAndStats extends StatelessWidget {
  const HereNowAndStats({
    Key key,
    @required this.venue,
  }) : super(key: key);

  final Venue venue;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          children: [
            Icon(
              Icons.beenhere,
              size: 16.0,
              color: Colors.grey[700],
            ),
            SizedBox(width: 8),
            Text(venue.hereNow.summary),
          ],
        ),
      ),
    );
  }
}

class PhotosList extends StatelessWidget {
  const PhotosList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<VenueDetail>(
      stream: BlocProvider.of<VenueDetailBloc>(context).venueDetailStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Column(
            children: snapshot.data.photos.groups
                .expand(
                  (element) => element.items.map(
                    (e) => Card(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: Row(
                          children: [
                            getAvatar(e.user),
                            SizedBox(width: 8.0),
                            Expanded(
                                child: Text(
                                    '${e.user.firstName} ${e.user.lastName}')),
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(4.0),
                                bottomRight: Radius.circular(4.0),
                              ),
                              child: FadeInImage.memoryNetwork(
                                image: e.size(
                                  width: 100,
                                  height: 100,
                                ),
                                placeholder: kTransparentImage,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
                .toList(),
          );
        } else {
          return Center(
            child: CustomCircularProgressIndicator(color: Colors.grey[700]),
          );
        }
      },
    );
  }
}

class TipsList extends StatelessWidget {
  const TipsList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<VenueDetail>(
      stream: BlocProvider.of<VenueDetailBloc>(context).venueDetailStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Column(
            children: snapshot.data.tips.groups
                .expand(
                  (element) => element.items.map(
                    (e) => Card(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: Row(
                          children: [
                            getAvatar(e.user),
                            SizedBox(width: 8.0),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      '${e.user.firstName} ${e.user.lastName}'),
                                  Text(
                                    e.text,
                                    style: TextStyle(
                                        fontStyle: FontStyle.italic,
                                        fontSize: 11.0,
                                        color: Colors.grey[700]),
                                  ),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.thumb_up,
                                        size: 16.0,
                                      ),
                                      Text('${e.agreeCount}'),
                                      SizedBox(
                                        width: 16.0,
                                      ),
                                      Icon(
                                        Icons.thumb_down,
                                        size: 16.0,
                                      ),
                                      Text('${e.disagreeCount}'),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            e.photo != null
                                ? ClipRRect(
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(4.0),
                                      bottomRight: Radius.circular(4.0),
                                    ),
                                    child: Image.network(e.photo.size(
                                      width: 100,
                                      height: 100,
                                    )),
                                  )
                                : SizedBox(width: 10, height: 10,),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
                .toList(),
          );
        } else {
          return Center(
            child: CustomCircularProgressIndicator(color: Colors.grey[700]),
          );
        }
      },
    );
  }
}

Widget getAvatar(User user) {
  return user.photo == null
      ? CircleAvatar(
          backgroundColor: Colors.green,
          child: Text(
            user.firstName[0].toUpperCase() ?? '' +
                user.lastName[0].toUpperCase() ?? '',
          ),
        )
      : CircleAvatar(
          backgroundImage:
              NetworkImage(user.photo.size(width: 100, height: 100)),
        );
}
