import 'package:json_annotation/json_annotation.dart';
import 'package:mysqr/data_layer/model/photo.dart';
import 'package:mysqr/data_layer/model/user.dart';

part 'photo_with_user.g.dart';

@JsonSerializable(explicitToJson: true)
class PhotoWithUser extends Photo {
  final User user;

  PhotoWithUser(String prefix, String suffix, this.user)
      : super(prefix, suffix);

  factory PhotoWithUser.fromJson(Map<String, dynamic> json) =>
      _$PhotoWithUserFromJson(json);

  Map<String, dynamic> toJson() => _$PhotoWithUserToJson(this);
}
