// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'photo_with_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhotoWithUser _$PhotoWithUserFromJson(Map<String, dynamic> json) {
  return PhotoWithUser(
    json['prefix'] as String,
    json['suffix'] as String,
    json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PhotoWithUserToJson(PhotoWithUser instance) =>
    <String, dynamic>{
      'prefix': instance.prefix,
      'suffix': instance.suffix,
      'user': instance.user?.toJson(),
    };
