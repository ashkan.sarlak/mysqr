import 'package:json_annotation/json_annotation.dart';

part 'phrase.g.dart';

@JsonSerializable(explicitToJson: true)
class Phrase {
  final String phrase;
  final Sample sample;

  Phrase(this.phrase, this.sample);

  factory Phrase.fromJson(Map<String, dynamic> json) => _$PhraseFromJson(json);

  Map<String, dynamic> toJson() => _$PhraseToJson(this);
}

@JsonSerializable()
class Sample {
  final String text;

  Sample(this.text);

  factory Sample.fromJson(Map<String, dynamic> json) => _$SampleFromJson(json);

  Map<String, dynamic> toJson() => _$SampleToJson(this);
}
