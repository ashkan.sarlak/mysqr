// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tips.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tips _$TipsFromJson(Map<String, dynamic> json) {
  return Tips(
    json['count'] as int,
    (json['groups'] as List)
        ?.map(
            (e) => e == null ? null : Group.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$TipsToJson(Tips instance) => <String, dynamic>{
      'count': instance.count,
      'groups': instance.groups?.map((e) => e?.toJson())?.toList(),
    };

Group _$GroupFromJson(Map<String, dynamic> json) {
  return Group(
    json['type'] as String,
    json['name'] as String,
    json['count'] as int,
    (json['items'] as List)
        ?.map((e) => e == null ? null : Tip.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$GroupToJson(Group instance) => <String, dynamic>{
      'type': instance.type,
      'name': instance.name,
      'count': instance.count,
      'items': instance.items?.map((e) => e?.toJson())?.toList(),
    };
