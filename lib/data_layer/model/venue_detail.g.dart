// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'venue_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VenueDetail _$VenueDetailFromJson(Map<String, dynamic> json) {
  return VenueDetail(
    json['id'] as String,
    json['name'] as String,
    (json['categories'] as List)
        ?.map((e) =>
            e == null ? null : Category.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['contact'] == null
        ? null
        : Contact.fromJson(json['contact'] as Map<String, dynamic>),
    json['bestPhoto'] == null
        ? null
        : Photo.fromJson(json['bestPhoto'] as Map<String, dynamic>),
    json['likes'] == null
        ? null
        : Likes.fromJson(json['likes'] as Map<String, dynamic>),
    json['photos'] == null
        ? null
        : Photos.fromJson(json['photos'] as Map<String, dynamic>),
    json['tips'] == null
        ? null
        : Tips.fromJson(json['tips'] as Map<String, dynamic>),
    (json['phrases'] as List)
        ?.map((e) =>
            e == null ? null : Phrase.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$VenueDetailToJson(VenueDetail instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'categories': instance.categories?.map((e) => e?.toJson())?.toList(),
      'contact': instance.contact?.toJson(),
      'bestPhoto': instance.bestPhoto?.toJson(),
      'likes': instance.likes?.toJson(),
      'photos': instance.photos?.toJson(),
      'tips': instance.tips?.toJson(),
      'phrases': instance.phrases?.map((e) => e?.toJson())?.toList(),
    };
