import 'package:json_annotation/json_annotation.dart';
import 'package:mysqr/data_layer/model/category.dart';
import 'package:mysqr/data_layer/model/likes.dart';
import 'package:mysqr/data_layer/model/photo.dart';
import 'package:mysqr/data_layer/model/photos.dart';
import 'package:mysqr/data_layer/model/phrase.dart';
import 'package:mysqr/data_layer/model/tips.dart';
import 'package:mysqr/data_layer/model/venue.dart';

part 'venue_detail.g.dart';

@JsonSerializable(explicitToJson: true)
class VenueDetail {
  final String id;
  final String name;
  final List<Category> categories;
  final Contact contact;
  final Photo bestPhoto;
  final Likes likes;
  final Photos photos;
  final Tips tips;
  final List<Phrase> phrases;

  VenueDetail(this.id, this.name, this.categories, this.contact, this.bestPhoto,
      this.likes, this.photos, this.tips, this.phrases);

  factory VenueDetail.fromJson(Map<String, dynamic> json) =>
      _$VenueDetailFromJson(json);

  Map<String, dynamic> toJson() => _$VenueDetailToJson(this);
}
