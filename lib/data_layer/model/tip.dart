import 'package:json_annotation/json_annotation.dart';
import 'package:mysqr/data_layer/model/likes.dart';
import 'package:mysqr/data_layer/model/photo.dart';
import 'package:mysqr/data_layer/model/user.dart';

part 'tip.g.dart';

@JsonSerializable(explicitToJson: true)
class Tip {
  final String text;
  final String type;
  final Photo photo;
  final Likes likes;
  final int agreeCount;
  final int disagreeCount;
  final User user;

  Tip(this.text, this.type, this.photo, this.likes, this.agreeCount,
      this.disagreeCount, this.user);

  factory Tip.fromJson(Map<String, dynamic> json) => _$TipFromJson(json);

  Map<String, dynamic> toJson() => _$TipToJson(this);
}
