// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'venue.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Venue _$VenueFromJson(Map<String, dynamic> json) {
  return Venue(
    json['id'] as String,
    json['name'] as String,
    json['location'] == null
        ? null
        : Location.fromJson(json['location'] as Map<String, dynamic>),
    (json['categories'] as List)
        ?.map((e) =>
            e == null ? null : Category.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['contact'] == null
        ? null
        : Contact.fromJson(json['contact'] as Map<String, dynamic>),
    json['verified'] as bool,
    json['stats'] == null
        ? null
        : Stats.fromJson(json['stats'] as Map<String, dynamic>),
    json['hereNow'] == null
        ? null
        : HereNow.fromJson(json['hereNow'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$VenueToJson(Venue instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'location': instance.location?.toJson(),
      'categories': instance.categories?.map((e) => e?.toJson())?.toList(),
      'contact': instance.contact?.toJson(),
      'verified': instance.verified,
      'stats': instance.stats?.toJson(),
      'hereNow': instance.hereNow?.toJson(),
    };

Contact _$ContactFromJson(Map<String, dynamic> json) {
  return Contact(
    json['phone'] as String,
    json['formattedPhone'] as String,
  );
}

Map<String, dynamic> _$ContactToJson(Contact instance) => <String, dynamic>{
      'phone': instance.phone,
      'formattedPhone': instance.formattedPhone,
    };

Stats _$StatsFromJson(Map<String, dynamic> json) {
  return Stats(
    json['tipCount'] as int,
    json['usersCount'] as int,
    json['checkinsCount'] as int,
  );
}

Map<String, dynamic> _$StatsToJson(Stats instance) => <String, dynamic>{
      'tipCount': instance.tipCount,
      'usersCount': instance.usersCount,
      'checkinsCount': instance.checkinsCount,
    };

HereNow _$HereNowFromJson(Map<String, dynamic> json) {
  return HereNow(
    json['count'] as int,
    json['summary'] as String,
  );
}

Map<String, dynamic> _$HereNowToJson(HereNow instance) => <String, dynamic>{
      'count': instance.count,
      'summary': instance.summary,
    };
