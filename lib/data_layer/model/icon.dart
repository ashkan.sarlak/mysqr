import 'package:json_annotation/json_annotation.dart';

part 'icon.g.dart';

@JsonSerializable()
class Icon {
  final String prefix;
  final String suffix;

  Icon(this.prefix, this.suffix);

  factory Icon.fromJson(Map<String, dynamic> json) => _$IconFromJson(json);

  Map<String, dynamic> toJson() => _$IconToJson(this);

  // For more info about icon url syntax see:
  // https://developer.foursquare.com/docs/api-reference/venues/categories/#response-fields

  String get noBg32 => size(32);

  String get noBg44 => size(44);

  String get noBg64 => size(64);

  String get noBg88 => size(88);

  String get bg32 => bgSize(32);

  String get bg44 => bgSize(44);

  String get bg64 => bgSize(64);

  String get bg88 => bgSize(88);

  String size(int size) => prefix + '$size' + suffix;

  String bgSize(int size) => prefix + 'bg_$size' + suffix;
}
