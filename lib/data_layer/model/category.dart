import 'package:json_annotation/json_annotation.dart';
import 'package:mysqr/data_layer/model/icon.dart';

part 'category.g.dart';

@JsonSerializable(explicitToJson: true)
class Category {
  final String id;
  final String name;
  final Icon icon;
  final bool primary;

  Category(this.id, this.name, this.icon, this.primary);

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}
