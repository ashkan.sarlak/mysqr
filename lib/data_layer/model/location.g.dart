// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Location _$LocationFromJson(Map<String, dynamic> json) {
  return Location(
    (json['lat'] as num)?.toDouble(),
    (json['lng'] as num)?.toDouble(),
    (json['formattedAddress'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$LocationToJson(Location instance) => <String, dynamic>{
      'lat': instance.lat,
      'lng': instance.lng,
      'formattedAddress': instance.formattedAddress,
    };
