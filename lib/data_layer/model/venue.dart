import 'package:json_annotation/json_annotation.dart';
import 'package:mysqr/data_layer/model/category.dart';
import 'package:mysqr/data_layer/model/location.dart';

part 'venue.g.dart';

@JsonSerializable(explicitToJson: true)
class Venue {
  final String id;
  final String name;
  final Location location;
  final List<Category> categories;
  final Contact contact;
  final bool verified;
  final Stats stats;
  final HereNow hereNow;

  Venue(this.id, this.name, this.location, this.categories, this.contact,
      this.verified, this.stats, this.hereNow);

  factory Venue.fromJson(Map<String, dynamic> json) => _$VenueFromJson(json);

  Map<String, dynamic> toJson() => _$VenueToJson(this);
}

@JsonSerializable()
class Contact {
  final String phone;
  final String formattedPhone;

  Contact(this.phone, this.formattedPhone);

  factory Contact.fromJson(Map<String, dynamic> json) =>
      _$ContactFromJson(json);

  Map<String, dynamic> toJson() => _$ContactToJson(this);
}

@JsonSerializable()
class Stats {
  final int tipCount;
  final int usersCount;
  final int checkinsCount;

  Stats(this.tipCount, this.usersCount, this.checkinsCount);

  factory Stats.fromJson(Map<String, dynamic> json) => _$StatsFromJson(json);

  Map<String, dynamic> toJson() => _$StatsToJson(this);
}

@JsonSerializable()
class HereNow {
  final int count;
  final String summary;

  HereNow(this.count, this.summary);

  factory HereNow.fromJson(Map<String, dynamic> json) =>
      _$HereNowFromJson(json);

  Map<String, dynamic> toJson() => _$HereNowToJson(this);
}
