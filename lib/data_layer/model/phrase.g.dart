// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phrase.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Phrase _$PhraseFromJson(Map<String, dynamic> json) {
  return Phrase(
    json['phrase'] as String,
    json['sample'] == null
        ? null
        : Sample.fromJson(json['sample'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PhraseToJson(Phrase instance) => <String, dynamic>{
      'phrase': instance.phrase,
      'sample': instance.sample?.toJson(),
    };

Sample _$SampleFromJson(Map<String, dynamic> json) {
  return Sample(
    json['text'] as String,
  );
}

Map<String, dynamic> _$SampleToJson(Sample instance) => <String, dynamic>{
      'text': instance.text,
    };
