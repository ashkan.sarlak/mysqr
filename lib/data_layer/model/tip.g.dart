// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tip.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tip _$TipFromJson(Map<String, dynamic> json) {
  return Tip(
    json['text'] as String,
    json['type'] as String,
    json['photo'] == null
        ? null
        : Photo.fromJson(json['photo'] as Map<String, dynamic>),
    json['likes'] == null
        ? null
        : Likes.fromJson(json['likes'] as Map<String, dynamic>),
    json['agreeCount'] as int,
    json['disagreeCount'] as int,
    json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TipToJson(Tip instance) => <String, dynamic>{
      'text': instance.text,
      'type': instance.type,
      'photo': instance.photo?.toJson(),
      'likes': instance.likes?.toJson(),
      'agreeCount': instance.agreeCount,
      'disagreeCount': instance.disagreeCount,
      'user': instance.user?.toJson(),
    };
