import 'package:json_annotation/json_annotation.dart';
import 'package:mysqr/data_layer/model/tip.dart';

part 'tips.g.dart';

@JsonSerializable(explicitToJson: true)
class Tips {
  final int count;
  final List<Group> groups;

  Tips(this.count, this.groups);

  factory Tips.fromJson(Map<String, dynamic> json) => _$TipsFromJson(json);

  Map<String, dynamic> toJson() => _$TipsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Group {
  final String type;
  final String name;
  final int count;
  final List<Tip> items;

  Group(this.type, this.name, this.count, this.items);

  factory Group.fromJson(Map<String, dynamic> json) => _$GroupFromJson(json);

  Map<String, dynamic> toJson() => _$GroupToJson(this);
}
