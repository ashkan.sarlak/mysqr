import 'package:json_annotation/json_annotation.dart';

part 'photo.g.dart';

@JsonSerializable()
class Photo {
  final String prefix;
  final String suffix;

  Photo(this.prefix, this.suffix);

  factory Photo.fromJson(Map<String, dynamic> json) => _$PhotoFromJson(json);

  Map<String, dynamic> toJson() => _$PhotoToJson(this);

  // For more info about photo url syntax see:
  // https://developer.foursquare.com/docs/api-reference/venues/photos/
  String get original => prefix + 'original' + suffix;

  String size({int width, int height}) => prefix + '${width}x$height' + suffix;
}
