// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'icon.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Icon _$IconFromJson(Map<String, dynamic> json) {
  return Icon(
    json['prefix'] as String,
    json['suffix'] as String,
  );
}

Map<String, dynamic> _$IconToJson(Icon instance) => <String, dynamic>{
      'prefix': instance.prefix,
      'suffix': instance.suffix,
    };
