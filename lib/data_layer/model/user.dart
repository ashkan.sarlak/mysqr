import 'package:json_annotation/json_annotation.dart';
import 'package:mysqr/data_layer/model/photo.dart';

part 'user.g.dart';

@JsonSerializable(explicitToJson: true)
class User {
  final String firstName;
  final String lastName;
  final String gender;
  final Photo photo;

  User(this.firstName, this.lastName, this.gender, this.photo);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
