import 'package:json_annotation/json_annotation.dart';
import 'package:mysqr/data_layer/model/photo_with_user.dart';

part 'photos.g.dart';

@JsonSerializable(explicitToJson: true)
class Photos {
  final int count;
  final List<Group> groups;

  Photos(this.count, this.groups);

  factory Photos.fromJson(Map<String, dynamic> json) => _$PhotosFromJson(json);

  Map<String, dynamic> toJson() => _$PhotosToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Group {
  final String type;
  final String name;
  final int count;
  final List<PhotoWithUser> items;

  Group(this.type, this.name, this.count, this.items);

  factory Group.fromJson(Map<String, dynamic> json) => _$GroupFromJson(json);

  Map<String, dynamic> toJson() => _$GroupToJson(this);
}
