import 'package:http/http.dart' as http;
import 'dart:convert' show json;
import 'package:meta/meta.dart';
import 'package:mysqr/bloc/adapter/map_data.dart';
import 'package:mysqr/data_layer/model/venue.dart';
import 'package:mysqr/data_layer/model/venue_detail.dart';
import 'package:mysqr/di/remote_client_contract.dart';

class FoursquareClient implements RemoteClientContract {
  final _host = 'api.foursquare.com';
  final _authParameters = {
    'client_id': 'VAU25WYCFWYBN1LWG0EGNR45Y0K3EE0OHC3AQVCMH0MFESXM',
    'client_secret': 'ADJ51G450X0UVKSZP1XRNP0R3KDQHUFXLER2MEBV0LWK1SZY',
    'v': '20180323',
  };

  Future<List<Venue>> fetchVenues(MapData mapData) async {
    final response = await request(path: 'v2/venues/search', parameters: {
      'll': mapData.userLatLng,
      'radius': mapData.searchRadius.toString(),
      'query': '""', // "query" is needed to activate "radius".
      // Also note that I used '""' instead of '', because the latter just omits
      // the parameter value.
    });

    return response['response']['venues']
        .map<Venue>((json) => Venue.fromJson(json))
        .toList(growable: false);
  }

  Future<VenueDetail> fetchVenueDetails(Venue venue) async {
    final response = await request(
      path: 'v2/venues/${venue.id}',
      parameters: {},
    );

    return VenueDetail.fromJson(response['response']['venue']);
  }

  Future<Map> request(
      {@required String path, Map<String, String> parameters}) async {
    parameters.addAll(_authParameters);
    final uri = Uri.https(_host, path, parameters);
    final result = await http.get(uri);
    final jsonObject = json.decode(result.body);
    return jsonObject;
  }
}
