import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mysqr/bloc/adapter/map_data.dart';
import 'package:mysqr/data_layer/model/venue.dart';
import 'package:permission_handler/permission_handler.dart';

import 'bloc/bloc_provider.dart';
import 'bloc/search_area_bloc.dart';

class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  GoogleMapController mapController;
  final LatLng _center = const LatLng(0, 0);
  static const double _defaultZoom = 17.0;
  List<Marker> _currentMarkers;

  SearchAreaBloc bloc;
  StreamSubscription searchButtonSubscription;
  StreamSubscription venueSelectedSubscription;

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<SearchAreaBloc>(context);
    searchButtonSubscription =
        bloc.searchButtonPressedStream.listen((_onSearchAreaButtonPressed));
    venueSelectedSubscription = bloc.selectedVenueStream.listen(
      (index) {
        print('map got the venue index: $index');
        if (index < 0) {
          return;
        }
        // When a Card is tapped, we move to its related marker manually.
        // NOTE.1: To avoid double moving, we consume tap event of markers.
        // So we move to tapped marker also manually.
        mapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target: _currentMarkers[index].position,
              zoom: _defaultZoom,
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    searchButtonSubscription.cancel();
    venueSelectedSubscription.cancel();
    super.dispose();
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;

    _askForLocationPermission();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Venue>>(
      stream: bloc.venueStream,
      builder: (context, snapshot) {
        _currentMarkers = List<Marker>();
        if (snapshot.hasData) {
          snapshot.data.asMap().forEach((index, element) {
            _currentMarkers.add(Marker(
              markerId: MarkerId(element.id),
              position: LatLng(element.location.lat, element.location.lng),
              // To avoid double moves. See NOTE.1 above.
              consumeTapEvents: true,
              onTap: () {
                print('$index: ${element.name}');
                bloc.selectVenue(index);
              },
            ));
          });
        }
        return GoogleMap(
          onMapCreated: _onMapCreated,
          mapToolbarEnabled: false,
          zoomControlsEnabled: false,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: _defaultZoom,
          ),
          markers: Set.from(_currentMarkers),
        );
      },
    );
  }

  Future<void> _onSearchAreaButtonPressed(data) async {
    var visibleRegion = await mapController.getVisibleRegion();
    var mapData = MapData(visibleRegion);
    bloc.queryMapData(mapData);
    print(
        'user\'s lat=${mapData.userLatitude} user\'s lng=${mapData.userLongitude}');
    print('search radius=${mapData.searchRadius}');
  }

  Future<void> _askForLocationPermission() async {
    if (await Permission.locationWhenInUse.request().isGranted) {
      final userCameraPosition = await _getUserCameraPosition();
      mapController.animateCamera(
        CameraUpdate.newCameraPosition(userCameraPosition),
      );
    } else {
      _showRationaleDialog();
    }
  }

  void _showRationaleDialog() {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Location Permission'),
          content: Text(
              'MySqr needs your permission to show your current location. Do you want to proceed?'),
          actions: <Widget>[
            TextButton(
              child: Text('Yes'),
              onPressed: () {
                Navigator.of(context).pop();
                _askForLocationPermission();
              },
            ),
            TextButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<CameraPosition> _getUserCameraPosition() async {
    final position = await Geolocator.getCurrentPosition();
    return CameraPosition(
      target: LatLng(position.latitude, position.longitude),
      zoom: _defaultZoom,
    );
  }
}
