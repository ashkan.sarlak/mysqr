import 'package:mysqr/bloc/adapter/map_data.dart';
import 'package:mysqr/data_layer/model/venue.dart';
import 'package:mysqr/data_layer/model/venue_detail.dart';

abstract class RemoteClientContract {
  Future<List<Venue>> fetchVenues(MapData mapData);

  Future<VenueDetail> fetchVenueDetails(Venue venue);
}
