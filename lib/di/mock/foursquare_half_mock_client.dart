import 'package:mysqr/bloc/adapter/map_data.dart';
import 'package:mysqr/data_layer/foursquare_client.dart';
import 'package:mysqr/data_layer/model/venue.dart';
import 'package:mysqr/data_layer/model/venue_detail.dart';
import 'package:mysqr/di/mock/sample_venue_detail.dart';
import 'package:mysqr/di/remote_client_contract.dart';

/// This is a half mock replacement class for FoursquareClient. One of its
/// methods return the actual response from the remote, the other returns
/// a pre-made object, if the real response is unavailable.
class FoursquareHalfMockClient implements RemoteClientContract {
  @override
  Future<List<Venue>> fetchVenues(MapData mapData) {
    return FoursquareClient().fetchVenues(mapData);
  }

  @override
  Future<VenueDetail> fetchVenueDetails(Venue venue) async {
    final result = await FoursquareClient().fetchVenueDetails(venue);
    if (result?.id == null) {
      // when the daily quota is over
      return sampleVenueDetail;
    } else {
      return result;
    }
  }
}
