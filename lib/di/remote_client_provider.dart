import 'package:mysqr/di/mock/foursquare_half_mock_client.dart';
import 'package:mysqr/di/remote_client_contract.dart';

class RemoteClientProvider {
  RemoteClientProvider._internal();

  static final _singleton = RemoteClientProvider._internal();

  factory RemoteClientProvider() {
    return _singleton;
  }

  RemoteClientContract get client => FoursquareHalfMockClient();
}
