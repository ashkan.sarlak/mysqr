import 'package:flutter/material.dart';

class CustomCircularProgressIndicator extends StatelessWidget {
  final double size;
  final double strokeWidth;
  final Color color;

  const CustomCircularProgressIndicator({
    Key key,
    this.size = 16.0,
    this.strokeWidth = 1.0,
    this.color = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: CircularProgressIndicator(
        strokeWidth: strokeWidth,
        valueColor: new AlwaysStoppedAnimation<Color>(color),
      ),
      width: size,
      height: size,
    );
  }
}