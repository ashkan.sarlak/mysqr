import 'package:flutter/material.dart';
import 'package:mysqr/bloc/bloc_provider.dart';
import 'package:mysqr/bloc/search_area_bloc.dart';
import 'package:mysqr/custom_circular_progress_indicator.dart';
import 'package:mysqr/foursquare_venues_list.dart';
import 'package:mysqr/map_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  /// NOTE: It seems when the tree is being rebuilt, the Bloc class in the
  /// BlocProvider is being re-instantiated. This may cause different users of
  /// the Bloc, to subscribe to different instances and break the program.
  /// For example when accessing Bloc in one Widget in the initState() and in
  /// another in the build() method. Hence here we use a final instance that
  /// remains the same on rebuilds.
  /// P.S. I experienced this at the beginning of the app when hot reloading/hot
  /// restarting and not on fresh restarts.
  final _searchAreaBloc = SearchAreaBloc();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green[900],
      ),
      home: Scaffold(
        body: BlocProvider(
          bloc: _searchAreaBloc,
          child: Stack(
            children: [
              MapScreen(),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 108,
                  margin: const EdgeInsets.only(bottom: 24),
                  child: FoursquareVenueList(),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: const EdgeInsets.only(top: 32),
                  child: SearchAreaButton(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SearchAreaButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<SearchAreaBloc>(context);

    return StreamBuilder<bool>(
        stream: bloc.isSearchingStream,
        builder: (context, snapshot) {
          final isSearching = snapshot.hasData && snapshot.data;
          return MaterialButton(
            color: Colors.green[700],
            elevation: 20,
            minWidth: 150,
            child: isSearching
                ? CustomCircularProgressIndicator()
                : Text(
                    'Search this area',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w300),
                  ),
            onPressed: isSearching
                ? () {}
                : () {
                    bloc.searchThisArea();
                  },
          );
        });
  }
}
